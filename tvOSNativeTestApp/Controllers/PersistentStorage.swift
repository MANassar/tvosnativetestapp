//
//  PersistentStorage.swift
//  MoviesAPI
//
//  Created by Mohamed Nassar on 12/06/2018.
//  Copyright © 2018 MohamedNassar. All rights reserved.
//

import Foundation

class PersistentStorage
{
    //
    // MARK:- Constants
    //
    private static let searchHistoryKey = "com.mnassar.careem.searchHistory"
    
    //
    // MARK:- Functions
    //
    class func saveSearchHistory(searchHistory:[String])
    {
        UserDefaults.standard.set(searchHistory, forKey: searchHistoryKey)
    }
    
    class func clearSearchHistory()
    {
        UserDefaults.standard.removeObject(forKey: searchHistoryKey)
    }
    
    class func getSearchHistory() -> [String]?
    {
        if let history = UserDefaults.standard.array(forKey: searchHistoryKey) as? [String]
        {
            return history
        }
        else
        {
            return nil
        }
    }
    
    class func updateSearchHistory(_ newItem:String) -> Bool
    {
        var searchHistory = self.getSearchHistory()
        if searchHistory == nil
        {
            searchHistory = [String]()
        }
        
        if !searchHistory!.contains(newItem) //Only update if we dont have the same string
        {
            searchHistory!.insert(newItem, at: 0) //Insert at the first position
            
            //Make sure search history is only 10 items. If so, delete the oldest item
            if searchHistory!.count > 10
            {
                searchHistory?.removeLast()
            }
            
            self.saveSearchHistory(searchHistory: searchHistory!)
            return true
        }
        else
        {
            return false
        }
    }
}
