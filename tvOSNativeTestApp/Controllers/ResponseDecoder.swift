//
//  MovieDecoder.swift
//  MoviesAPI
//
//  Created by Mohamed Nassar on 07/06/2018.
//  Copyright © 2018 MohamedNassar. All rights reserved.
//

import Foundation


class ResponseDecoder
{
    //
    // MARK:- Contants and refs
    //
    static let decoder = JSONDecoder()
    
    //
    // MARK:- Decoding functions
    //    
    class func decodeResponseJSONData(_ jsonData:Data) -> MovieResponse?
    {
        if let decodedResponse = try? decoder.decode(MovieResponse.self, from: jsonData)
        {
            return decodedResponse
        }
        else
        {
            return nil
        }
    }
}
