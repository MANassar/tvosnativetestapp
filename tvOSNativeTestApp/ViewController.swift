//
//  ViewController.swift
//  tvOSNativeTestApp
//
//  Created by Nassar, Mohamed on 17.10.18.
//  Copyright © 2018 NassarCorp. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDelegate, UITableViewDataSource
{
    @IBOutlet var collectionView: UICollectionView!
    let superheroArray = ["Batman", "Superman", "Spiderman", "Flash"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return superheroArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cvID", for: indexPath) as? CollectionCell
        {
            cell.textLabel.text = superheroArray[indexPath.row]
            return cell
        }
        else
        {
            return UICollectionViewCell()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didUpdateFocusIn context: UICollectionViewFocusUpdateContext, with coordinator: UIFocusAnimationCoordinator)
    {
        if let previousIndexPath = context.previouslyFocusedIndexPath,
            let cell = collectionView.cellForItem(at: previousIndexPath) {
            cell.contentView.layer.borderWidth = 0.0
            cell.contentView.layer.shadowRadius = 0.0
            cell.contentView.layer.shadowOpacity = 0
        }
        
        if let indexPath = context.nextFocusedIndexPath,
            let cell = collectionView.cellForItem(at: indexPath) {
            cell.contentView.layer.borderWidth = 8.0
            cell.contentView.layer.borderColor = UIColor.white.cgColor
            cell.contentView.layer.shadowColor = UIColor.white.cgColor
            cell.contentView.layer.shadowRadius = 10.0
            cell.contentView.layer.shadowOpacity = 0.9
            cell.contentView.layer.shadowOffset = CGSize(width: 0, height: 0)
            collectionView.scrollToItem(at: indexPath, at: [.centeredHorizontally, .centeredVertically], animated: true)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        handleSelection(indexPath)
    }
    
    //
    // MARK:- TableView
    //
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return superheroArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableCell")
        cell?.textLabel?.text = superheroArray[indexPath.row]
        return cell ?? UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        handleSelection(indexPath)
    }
    
    fileprivate func handleSelection(_ indexPath: IndexPath)
    {
        var searchQuery = ""
        switch indexPath.row {
        case 0:
            searchQuery = "Batman"
        case 1:
            searchQuery = "Superman"
        case 2:
            searchQuery = "Spiderman"
        default:
            searchQuery = "Flash"
        }
        
        API_Interface.getMovies(searchQuery: searchQuery) { (response, error) in
            let alertController = UIAlertController(title: "Got results", message: "\(response!.total_results!) \(searchQuery) movies", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (action) in
                self.dismiss(animated: true, completion: nil)
            })
            alertController.addAction(okAction)
            let newVC = UIViewController()
            self.navigationController?.pushViewController(newVC, animated: true)
            newVC.present(alertController, animated: true, completion: nil)
        }
    }
}

class CollectionCell:UICollectionViewCell
{
    @IBOutlet var textLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    
}
